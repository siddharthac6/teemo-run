let gulp = require("gulp");
let shell = require('gulp-shell');

gulp.task('browserify-changes', shell.task('browserify app/assets/js/main.js -o app/assets/js/build.js -d'));

gulp.task('watch', function() {
    gulp.watch('app/assets/**/*.js', gulp.series( 'browserify-changes'));
});
  