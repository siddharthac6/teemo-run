var $ = require("./util.js");
var $scene = require("./scene.js");
var $player = require("./player.js");
var $enemy = require("./enemy.js");

(function(){
    $.renderer.backgroundColor = 0xffffff;
    $.stage.interactive = true;

    window.addEventListener("keydown", event => onKeyDown(event));

    loadResource();
    document.body.appendChild($.renderer.view);
}());

function loadResource() {
    $.loader.add('/sprites/sprites.json');
    $.loader.on('progress', (_loader, resource) => {
        console.log(`Loading: ${resource.name} (${_loader.progress}%)`);
    });
    $.loader.load((loader, resources) => {
        console.log("All files loaded ...");
        $scene.setupScene();
        $player.setupPlayer();
        $enemy.setupEnemy();
        update();
    });
    $.loader.onComplete.add(() => {
        console.log("Loading Complete ...")
        $.stage.addChild($.scene);
        $.stage.addChild($.player);

        $player.walkingPlayer();
    });
}

function onKeyDown (event) {
    switch(event.keyCode) {
        // case 40:
        //     rollingPlayer();
        //     break;
        // case 38:
        //     jumpingPlayer();
        //     break;
        case 39:
            // Right arrow key
            $player.shootingPlayer();
            break;
        case 37:
            // Left arrow key
            $enemy.placeEnemy();
            break;
    }
    
}

function update() {
   

    $.updateScene();
    $.updatePlayerPosition();

    $.renderer.render($.stage);
    window.requestAnimationFrame(update);
    
}