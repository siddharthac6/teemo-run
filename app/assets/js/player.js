var $ = require("./util.js");

function walkingPlayer () {
    $.setPlayerAnimation($.animationState.RUNNING);
    let textureArrayRunning = [];

    for (let i = 1; i <= 2; i++) {
        textureArrayRunning.push($.TextureCache['walk'+i+'.png']);
    }

    let walkingSprite = new PIXI.extras.AnimatedSprite(textureArrayRunning);
    
    walkingSprite.animationSpeed = (.10 + $.state.timeSlow);
    walkingSprite.play();

    flushPlayerContainer();
    $.player.addChild(walkingSprite);
}

function shootingPlayer () {
    if ($.state.playerInfo.activeAnimation !== $.animationState.RUNNING) {
        return;
    }

    let darts = $.state.darts;
    if (darts.activeDarts > 1) {
        return;
    }

    console.log("darts.activeDarts", $.scene.children)

    darts.activeDarts++;
    $.setState({darts});

    $.setPlayerAnimation($.animationState.SHOOTING);

    let textureArrayShooting = [];
    for (let i = 1; i <= 2; i++) {
        textureArrayShooting.push($.TextureCache['shoot.png']);
    }

    let shootingSprite = new PIXI.extras.AnimatedSprite(textureArrayShooting);

    shootingSprite.animationSpeed = (.01 + $.state.timeSlow);
    shootingSprite.play();

    flushPlayerContainer();
    $.player.addChild(shootingSprite);

    shootDart();
    walkingPlayer();
}

function shootDart () {

    let dart = new $.Sprite($.TextureCache["dart.png"]);
    let dartspeed = $.state.darts.dartspeed;
    
    let dartScalePoint = new PIXI.Point(0.5, 0.5);
    dart.scale = dartScalePoint;

    let playerRotation = $.player.rotation;
    let dartStartPosition = {
        x: $.player.position.x + Math.cos(playerRotation)*20,
        y: $.player.position.y + Math.sin(playerRotation)*20
    };

    dart.position.x = dartStartPosition.x + 20;
    dart.position.y = dartStartPosition.y - 40;
    dart.rotation = playerRotation;

    var smoothie = new Smoothie({
        engine: PIXI, 
        renderer: $.renderer,
        root: $.stage,
        fps: 30,
        update: update.bind(this)
    });

    
    $.stage.addChild(dart);
    smoothie.start();

    function update () {
        if (dart.position.x > $.stageWidth) {
            $.stage.removeChild(dart);

            let darts = $.state.darts;
            darts.activeDarts --;
            $.setState({darts});
        } else {
            dart.position.x += Math.cos(dart.rotation) * dartspeed;
            dart.position.y += Math.sin(dart.rotation) * dartspeed;
        }
    }

}
// Player utility function below:

function flushPlayerContainer() {
    for (let i = 0; i < $.player.children.length; i++) {
        $.player.removeChildAt(i);
    }
}

function setupPlayer () {
    $.player.anchor.x = 0.5;
    $.player.anchor.y = 0.5;

    $.player.pivot.x = 0.5;
    $.player.pivot.y = 200; // 200

    $.player.scale.x  = 0.30;
    $.player.scale.y  = 0.30;
}

module.exports = {
    setupPlayer,
    refreshPlayer: setupPlayer,

    walkingPlayer,
    defaultPlayer: walkingPlayer,
    shootingPlayer,
}