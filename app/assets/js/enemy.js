var $ = require("./util.js");

function setupEnemy () {
// 
}


function placeEnemy () {
    console.log("Placing enemy");
    addHedgeHog();
}


function addHedgeHog  () {
    let hedgeHog = initializeHedgeHog(new $.Sprite($.TextureCache["hedgehog.png"]));
    var smoothie = new Smoothie({
        engine: PIXI, 
        renderer: $.renderer,
        root: $.stage,
        fps: 30,
        update: update.bind(this)
    });

    function update() {
        if (hedgeHog.x < (0 - hedgeHog.width)) {
            $.scene.removeChild(hedgeHog)
        }
        hedgeHog.x -= ($.state.scene.road.movementSpeed + $.state.timeSlow) *2;
    }

    $.scene.addChild(hedgeHog);
    $.state.activeEnemies.push(hedgeHog);
    
    smoothie.start();
}

function initializeHedgeHog (hedgeHog) {
    hedgeHog.scale = new PIXI.Point(0.15, 0.15);
    hedgeHog.anchor.set(0, 1)
    hedgeHog.position.x = $.stageWidth + hedgeHog.width;
    hedgeHog.position.y = $.state.scene.road.position.y;

    return hedgeHog;
}

function initializeSpider () {
    let spider = new $.Sprite($.TextureCache["spider.png"]);

    spider.scale = new PIXI.Point(2, 2);
    spider.anchor.set(0, 1);
    spider.position.x = $.stageWidth + 20;
    spider.position.y = $.state.scene.road.position.y;
    
    $.state.sprites.spider = spider;
}

module.exports = {
    setupEnemy,
    placeEnemy,
}



// var enemy = ["bat", "hedge-hog", "cactus", "spider"]
// var enemyBalancing = {
//     "hedge-hog": {
//         scale: {
//             x: 2,
//             y: 2,
//         },
//         movementSpeed: $.state.scene.road.movementSpeed,
//         position: {
//             x: $.stageWidth + 20,
//             y: $.state.scene.road.position.y,
//         },
//         pivot: {
//             x: 0,
//             y: 200,
//         }
//     },
    
//     "spider": {
//         scale: {
//             x: 2,
//             y: 2,
//         },
//         movementSpeed: $.state.scene.road.movementSpeed,
//         position: {
//             x: $.stageWidth + 20,
//             y: $.state.scene.road.position.y,
//         },
//         pivot: {
//             x: 0,
//             y: 200,
//         }
//     },

//     "cactus": {

//     }
// };