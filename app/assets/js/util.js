var
stageWidth              = 700,
stageHeight             = 400,
Container               = PIXI.Container,
TextureCache            = PIXI.utils.TextureCache,
TilingSprite            = PIXI.TilingSprite,
Sprite                  = PIXI.Sprite,
autoDetectRenderer      = PIXI.autoDetectRenderer,
loader                  = PIXI.loader,
resources               = PIXI.loader.resources,
renderer                = autoDetectRenderer(stageWidth, stageHeight),
stage                   = new Container(),
scene                   = new Container(),
player                  = new Sprite();

const animationState = {
    RUNNING: "RUNNING",
    JUMPING: "JUMPING",
    ROLLING: "ROLLING",
    SHOOTING: "SHOOTING",
    SLOW: "SLOW",
};

var gameState = {
    sprites: {},
    timeSlow: 0,
    timers: [],
    darts: {
        activeDarts: 0,
        dartspeed: 10,
    },
    activeEnemies: [],
    playerInfo: {
        activeAnimation: animationState.RUNNING, // active player animation state
        jumpDelta: 5,
        rollDelta: 15,
        height: 0,
        width: 0,
        position:{
            x: 100,
            y: 251 // `road` posiiton y
        }
    },
    scene: {
        road: {
            position: {
                x: 0,
                y: 251,
            },
            movementSpeed: 2
        },
        clouds: {
            position: {
                x: 0,
                y: 0,
            },
            movementSpeed: 0.3
        },
        hills: {
            position: {
                x: 0,
                y: 0,
            },
            movementSpeed: 0.2
        }
    }
}

var state = gameState;
var setState = (change = null) => {
    if(!change) {
        state = gameState;
    } else {
        state = Object.assign({}, state, change);
    }
    return;
}

var setPlayerAnimation = (animationState) => {
    let playerInfo = state.playerInfo;
    playerInfo.activeAnimation = animationState;
}

var updatePlayerPosition = () => {
    let playerInfo = state.playerInfo;
    
    player.position.x = playerInfo.position.x;
    player.position.y = playerInfo.position.y;
}


var updateScene = () => {
    let scene = state.scene;

    state.sprites.hills.tilePosition.x -= (scene.hills.movementSpeed + state.timeSlow);
    state.sprites.clouds.tilePosition.x -= (scene.clouds.movementSpeed + state.timeSlow);
    state.sprites.road.tilePosition.x -= (scene.road.movementSpeed + state.timeSlow);
}

module.exports = {
    stageWidth              ,
    stageHeight             ,
    Container               ,
    TextureCache            ,
    TilingSprite            ,
    Sprite                  ,
    autoDetectRenderer      ,
    loader                  ,
    renderer                ,
    resources               ,
    stage                   ,
    scene                   ,
    player                  ,
    animationState          ,
    state                   ,
    setState                ,
    setPlayerAnimation      ,
    updatePlayerPosition    ,
    updateScene             ,
}