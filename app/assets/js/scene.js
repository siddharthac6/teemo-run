var $ = require("./util.js");

function setupScene () {
    let scene = $.state.scene;

    let hills = $.TextureCache["hills.png"];
    $.state.sprites.hills = new PIXI.TilingSprite(hills, 1513, 252);
    addToScene($.state.sprites.hills, scene.hills.position, {x: 0, y: 0});

    let clouds = $.TextureCache["clouds.png"];
    $.state.sprites.clouds = new PIXI.TilingSprite(clouds, 1513, 100);
    addToScene($.state.sprites.clouds, scene.clouds.position, {x: 0, y: 0});

    let road = $.TextureCache["road.png"];
    $.state.sprites.road = new PIXI.TilingSprite(road, 1513, 52);
    addToScene($.state.sprites.road, scene.road.position, {x: 0, y: 0});
}

function addToScene(sprite, position, tilePosition = null) {
    sprite.position.x = position.x;
    sprite.position.y = position.y;

    if (tilePosition !== null ) {
        sprite.tilePosition.x = tilePosition.x;
        sprite.tilePosition.y = tilePosition.y;
    }
    
    $.scene.addChild(sprite);
}

module.exports = {
    setupScene,
}