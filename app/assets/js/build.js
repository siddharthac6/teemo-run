(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
var $ = require("./util.js");

function setupEnemy () {
// 
}


function placeEnemy () {
    console.log("Placing enemy");
    addHedgeHog();
}


function addHedgeHog  () {
    let hedgeHog = initializeHedgeHog(new $.Sprite($.TextureCache["hedgehog.png"]));
    var smoothie = new Smoothie({
        engine: PIXI, 
        renderer: $.renderer,
        root: $.stage,
        fps: 30,
        update: update.bind(this)
    });

    function update() {
        if (hedgeHog.x < (0 - hedgeHog.width)) {
            $.scene.removeChild(hedgeHog)
        }
        hedgeHog.x -= ($.state.scene.road.movementSpeed + $.state.timeSlow) *2;
    }

    $.scene.addChild(hedgeHog);
    $.state.activeEnemies.push(hedgeHog);
    
    smoothie.start();
}

function initializeHedgeHog (hedgeHog) {
    hedgeHog.scale = new PIXI.Point(0.15, 0.15);
    hedgeHog.anchor.set(0, 1)
    hedgeHog.position.x = $.stageWidth + hedgeHog.width;
    hedgeHog.position.y = $.state.scene.road.position.y;

    return hedgeHog;
}

function initializeSpider () {
    let spider = new $.Sprite($.TextureCache["spider.png"]);

    spider.scale = new PIXI.Point(2, 2);
    spider.anchor.set(0, 1);
    spider.position.x = $.stageWidth + 20;
    spider.position.y = $.state.scene.road.position.y;
    
    $.state.sprites.spider = spider;
}

module.exports = {
    setupEnemy,
    placeEnemy,
}



// var enemy = ["bat", "hedge-hog", "cactus", "spider"]
// var enemyBalancing = {
//     "hedge-hog": {
//         scale: {
//             x: 2,
//             y: 2,
//         },
//         movementSpeed: $.state.scene.road.movementSpeed,
//         position: {
//             x: $.stageWidth + 20,
//             y: $.state.scene.road.position.y,
//         },
//         pivot: {
//             x: 0,
//             y: 200,
//         }
//     },
    
//     "spider": {
//         scale: {
//             x: 2,
//             y: 2,
//         },
//         movementSpeed: $.state.scene.road.movementSpeed,
//         position: {
//             x: $.stageWidth + 20,
//             y: $.state.scene.road.position.y,
//         },
//         pivot: {
//             x: 0,
//             y: 200,
//         }
//     },

//     "cactus": {

//     }
// };
},{"./util.js":5}],2:[function(require,module,exports){
var $ = require("./util.js");
var $scene = require("./scene.js");
var $player = require("./player.js");
var $enemy = require("./enemy.js");

(function(){
    $.renderer.backgroundColor = 0xffffff;
    $.stage.interactive = true;

    window.addEventListener("keydown", event => onKeyDown(event));

    loadResource();
    document.body.appendChild($.renderer.view);
}());

function loadResource() {
    $.loader.add('/sprites/sprites.json');
    $.loader.on('progress', (_loader, resource) => {
        console.log(`Loading: ${resource.name} (${_loader.progress}%)`);
    });
    $.loader.load((loader, resources) => {
        console.log("All files loaded ...");
        $scene.setupScene();
        $player.setupPlayer();
        $enemy.setupEnemy();
        update();
    });
    $.loader.onComplete.add(() => {
        console.log("Loading Complete ...")
        $.stage.addChild($.scene);
        $.stage.addChild($.player);

        $player.walkingPlayer();
    });
}

function onKeyDown (event) {
    switch(event.keyCode) {
        // case 40:
        //     rollingPlayer();
        //     break;
        // case 38:
        //     jumpingPlayer();
        //     break;
        case 39:
            // Right arrow key
            $player.shootingPlayer();
            break;
        case 37:
            // Left arrow key
            $enemy.placeEnemy();
            break;
    }
    
}

function update() {
   

    $.updateScene();
    $.updatePlayerPosition();

    $.renderer.render($.stage);
    window.requestAnimationFrame(update);
    
}
},{"./enemy.js":1,"./player.js":3,"./scene.js":4,"./util.js":5}],3:[function(require,module,exports){
var $ = require("./util.js");

function walkingPlayer () {
    $.setPlayerAnimation($.animationState.RUNNING);
    let textureArrayRunning = [];

    for (let i = 1; i <= 2; i++) {
        textureArrayRunning.push($.TextureCache['walk'+i+'.png']);
    }

    let walkingSprite = new PIXI.extras.AnimatedSprite(textureArrayRunning);
    
    walkingSprite.animationSpeed = (.10 + $.state.timeSlow);
    walkingSprite.play();

    flushPlayerContainer();
    $.player.addChild(walkingSprite);
}

function shootingPlayer () {
    if ($.state.playerInfo.activeAnimation !== $.animationState.RUNNING) {
        return;
    }

    let darts = $.state.darts;
    if (darts.activeDarts > 1) {
        return;
    }

    console.log("darts.activeDarts", $.scene.children)

    darts.activeDarts++;
    $.setState({darts});

    $.setPlayerAnimation($.animationState.SHOOTING);

    let textureArrayShooting = [];
    for (let i = 1; i <= 2; i++) {
        textureArrayShooting.push($.TextureCache['shoot.png']);
    }

    let shootingSprite = new PIXI.extras.AnimatedSprite(textureArrayShooting);

    shootingSprite.animationSpeed = (.01 + $.state.timeSlow);
    shootingSprite.play();

    flushPlayerContainer();
    $.player.addChild(shootingSprite);

    shootDart();
    walkingPlayer();
}

function shootDart () {

    let dart = new $.Sprite($.TextureCache["dart.png"]);
    let dartspeed = $.state.darts.dartspeed;
    
    let dartScalePoint = new PIXI.Point(0.5, 0.5);
    dart.scale = dartScalePoint;

    let playerRotation = $.player.rotation;
    let dartStartPosition = {
        x: $.player.position.x + Math.cos(playerRotation)*20,
        y: $.player.position.y + Math.sin(playerRotation)*20
    };

    dart.position.x = dartStartPosition.x + 20;
    dart.position.y = dartStartPosition.y - 40;
    dart.rotation = playerRotation;

    var smoothie = new Smoothie({
        engine: PIXI, 
        renderer: $.renderer,
        root: $.stage,
        fps: 30,
        update: update.bind(this)
    });

    
    $.stage.addChild(dart);
    smoothie.start();

    function update () {
        if (dart.position.x > $.stageWidth) {
            $.stage.removeChild(dart);

            let darts = $.state.darts;
            darts.activeDarts --;
            $.setState({darts});
        } else {
            dart.position.x += Math.cos(dart.rotation) * dartspeed;
            dart.position.y += Math.sin(dart.rotation) * dartspeed;
        }
    }

}
// Player utility function below:

function flushPlayerContainer() {
    for (let i = 0; i < $.player.children.length; i++) {
        $.player.removeChildAt(i);
    }
}

function setupPlayer () {
    $.player.anchor.x = 0.5;
    $.player.anchor.y = 0.5;

    $.player.pivot.x = 0.5;
    $.player.pivot.y = 200; // 200

    $.player.scale.x  = 0.30;
    $.player.scale.y  = 0.30;
}

module.exports = {
    setupPlayer,
    refreshPlayer: setupPlayer,

    walkingPlayer,
    defaultPlayer: walkingPlayer,
    shootingPlayer,
}
},{"./util.js":5}],4:[function(require,module,exports){
var $ = require("./util.js");

function setupScene () {
    let scene = $.state.scene;

    let hills = $.TextureCache["hills.png"];
    $.state.sprites.hills = new PIXI.TilingSprite(hills, 1513, 252);
    addToScene($.state.sprites.hills, scene.hills.position, {x: 0, y: 0});

    let clouds = $.TextureCache["clouds.png"];
    $.state.sprites.clouds = new PIXI.TilingSprite(clouds, 1513, 100);
    addToScene($.state.sprites.clouds, scene.clouds.position, {x: 0, y: 0});

    let road = $.TextureCache["road.png"];
    $.state.sprites.road = new PIXI.TilingSprite(road, 1513, 52);
    addToScene($.state.sprites.road, scene.road.position, {x: 0, y: 0});
}

function addToScene(sprite, position, tilePosition = null) {
    sprite.position.x = position.x;
    sprite.position.y = position.y;

    if (tilePosition !== null ) {
        sprite.tilePosition.x = tilePosition.x;
        sprite.tilePosition.y = tilePosition.y;
    }
    
    $.scene.addChild(sprite);
}

module.exports = {
    setupScene,
}
},{"./util.js":5}],5:[function(require,module,exports){
var
stageWidth              = 700,
stageHeight             = 400,
Container               = PIXI.Container,
TextureCache            = PIXI.utils.TextureCache,
TilingSprite            = PIXI.TilingSprite,
Sprite                  = PIXI.Sprite,
autoDetectRenderer      = PIXI.autoDetectRenderer,
loader                  = PIXI.loader,
resources               = PIXI.loader.resources,
renderer                = autoDetectRenderer(stageWidth, stageHeight),
stage                   = new Container(),
scene                   = new Container(),
player                  = new Sprite();

const animationState = {
    RUNNING: "RUNNING",
    JUMPING: "JUMPING",
    ROLLING: "ROLLING",
    SHOOTING: "SHOOTING",
    SLOW: "SLOW",
};

var gameState = {
    sprites: {},
    timeSlow: 0,
    timers: [],
    darts: {
        activeDarts: 0,
        dartspeed: 10,
    },
    activeEnemies: [],
    playerInfo: {
        activeAnimation: animationState.RUNNING, // active player animation state
        jumpDelta: 5,
        rollDelta: 15,
        height: 0,
        width: 0,
        position:{
            x: 100,
            y: 251 // `road` posiiton y
        }
    },
    scene: {
        road: {
            position: {
                x: 0,
                y: 251,
            },
            movementSpeed: 2
        },
        clouds: {
            position: {
                x: 0,
                y: 0,
            },
            movementSpeed: 0.3
        },
        hills: {
            position: {
                x: 0,
                y: 0,
            },
            movementSpeed: 0.2
        }
    }
}

var state = gameState;
var setState = (change = null) => {
    if(!change) {
        state = gameState;
    } else {
        state = Object.assign({}, state, change);
    }
    return;
}

var setPlayerAnimation = (animationState) => {
    let playerInfo = state.playerInfo;
    playerInfo.activeAnimation = animationState;
}

var updatePlayerPosition = () => {
    let playerInfo = state.playerInfo;
    
    player.position.x = playerInfo.position.x;
    player.position.y = playerInfo.position.y;
}


var updateScene = () => {
    let scene = state.scene;

    state.sprites.hills.tilePosition.x -= (scene.hills.movementSpeed + state.timeSlow);
    state.sprites.clouds.tilePosition.x -= (scene.clouds.movementSpeed + state.timeSlow);
    state.sprites.road.tilePosition.x -= (scene.road.movementSpeed + state.timeSlow);
}

module.exports = {
    stageWidth              ,
    stageHeight             ,
    Container               ,
    TextureCache            ,
    TilingSprite            ,
    Sprite                  ,
    autoDetectRenderer      ,
    loader                  ,
    renderer                ,
    resources               ,
    stage                   ,
    scene                   ,
    player                  ,
    animationState          ,
    state                   ,
    setState                ,
    setPlayerAnimation      ,
    updatePlayerPosition    ,
    updateScene             ,
}
},{}]},{},[2])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL1VzZXJzL3NjaG93ZGh1cnkvQXBwRGF0YS9Sb2FtaW5nL25wbS9ub2RlX21vZHVsZXMvYnJvd3NlcmlmeS9ub2RlX21vZHVsZXMvYnJvd3Nlci1wYWNrL19wcmVsdWRlLmpzIiwiYXBwL2Fzc2V0cy9qcy9lbmVteS5qcyIsImFwcC9hc3NldHMvanMvbWFpbi5qcyIsImFwcC9hc3NldHMvanMvcGxheWVyLmpzIiwiYXBwL2Fzc2V0cy9qcy9zY2VuZS5qcyIsImFwcC9hc3NldHMvanMvdXRpbC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcEdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNqRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDM0hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKXtmdW5jdGlvbiByKGUsbix0KXtmdW5jdGlvbiBvKGksZil7aWYoIW5baV0pe2lmKCFlW2ldKXt2YXIgYz1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlO2lmKCFmJiZjKXJldHVybiBjKGksITApO2lmKHUpcmV0dXJuIHUoaSwhMCk7dmFyIGE9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitpK1wiJ1wiKTt0aHJvdyBhLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsYX12YXIgcD1uW2ldPXtleHBvcnRzOnt9fTtlW2ldWzBdLmNhbGwocC5leHBvcnRzLGZ1bmN0aW9uKHIpe3ZhciBuPWVbaV1bMV1bcl07cmV0dXJuIG8obnx8cil9LHAscC5leHBvcnRzLHIsZSxuLHQpfXJldHVybiBuW2ldLmV4cG9ydHN9Zm9yKHZhciB1PVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmUsaT0wO2k8dC5sZW5ndGg7aSsrKW8odFtpXSk7cmV0dXJuIG99cmV0dXJuIHJ9KSgpIiwidmFyICQgPSByZXF1aXJlKFwiLi91dGlsLmpzXCIpO1xyXG5cclxuZnVuY3Rpb24gc2V0dXBFbmVteSAoKSB7XHJcbi8vIFxyXG59XHJcblxyXG5cclxuZnVuY3Rpb24gcGxhY2VFbmVteSAoKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIlBsYWNpbmcgZW5lbXlcIik7XHJcbiAgICBhZGRIZWRnZUhvZygpO1xyXG59XHJcblxyXG5cclxuZnVuY3Rpb24gYWRkSGVkZ2VIb2cgICgpIHtcclxuICAgIGxldCBoZWRnZUhvZyA9IGluaXRpYWxpemVIZWRnZUhvZyhuZXcgJC5TcHJpdGUoJC5UZXh0dXJlQ2FjaGVbXCJoZWRnZWhvZy5wbmdcIl0pKTtcclxuICAgIHZhciBzbW9vdGhpZSA9IG5ldyBTbW9vdGhpZSh7XHJcbiAgICAgICAgZW5naW5lOiBQSVhJLCBcclxuICAgICAgICByZW5kZXJlcjogJC5yZW5kZXJlcixcclxuICAgICAgICByb290OiAkLnN0YWdlLFxyXG4gICAgICAgIGZwczogMzAsXHJcbiAgICAgICAgdXBkYXRlOiB1cGRhdGUuYmluZCh0aGlzKVxyXG4gICAgfSk7XHJcblxyXG4gICAgZnVuY3Rpb24gdXBkYXRlKCkge1xyXG4gICAgICAgIGlmIChoZWRnZUhvZy54IDwgKDAgLSBoZWRnZUhvZy53aWR0aCkpIHtcclxuICAgICAgICAgICAgJC5zY2VuZS5yZW1vdmVDaGlsZChoZWRnZUhvZylcclxuICAgICAgICB9XHJcbiAgICAgICAgaGVkZ2VIb2cueCAtPSAoJC5zdGF0ZS5zY2VuZS5yb2FkLm1vdmVtZW50U3BlZWQgKyAkLnN0YXRlLnRpbWVTbG93KSAqMjtcclxuICAgIH1cclxuXHJcbiAgICAkLnNjZW5lLmFkZENoaWxkKGhlZGdlSG9nKTtcclxuICAgICQuc3RhdGUuYWN0aXZlRW5lbWllcy5wdXNoKGhlZGdlSG9nKTtcclxuICAgIFxyXG4gICAgc21vb3RoaWUuc3RhcnQoKTtcclxufVxyXG5cclxuZnVuY3Rpb24gaW5pdGlhbGl6ZUhlZGdlSG9nIChoZWRnZUhvZykge1xyXG4gICAgaGVkZ2VIb2cuc2NhbGUgPSBuZXcgUElYSS5Qb2ludCgwLjE1LCAwLjE1KTtcclxuICAgIGhlZGdlSG9nLmFuY2hvci5zZXQoMCwgMSlcclxuICAgIGhlZGdlSG9nLnBvc2l0aW9uLnggPSAkLnN0YWdlV2lkdGggKyBoZWRnZUhvZy53aWR0aDtcclxuICAgIGhlZGdlSG9nLnBvc2l0aW9uLnkgPSAkLnN0YXRlLnNjZW5lLnJvYWQucG9zaXRpb24ueTtcclxuXHJcbiAgICByZXR1cm4gaGVkZ2VIb2c7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGluaXRpYWxpemVTcGlkZXIgKCkge1xyXG4gICAgbGV0IHNwaWRlciA9IG5ldyAkLlNwcml0ZSgkLlRleHR1cmVDYWNoZVtcInNwaWRlci5wbmdcIl0pO1xyXG5cclxuICAgIHNwaWRlci5zY2FsZSA9IG5ldyBQSVhJLlBvaW50KDIsIDIpO1xyXG4gICAgc3BpZGVyLmFuY2hvci5zZXQoMCwgMSk7XHJcbiAgICBzcGlkZXIucG9zaXRpb24ueCA9ICQuc3RhZ2VXaWR0aCArIDIwO1xyXG4gICAgc3BpZGVyLnBvc2l0aW9uLnkgPSAkLnN0YXRlLnNjZW5lLnJvYWQucG9zaXRpb24ueTtcclxuICAgIFxyXG4gICAgJC5zdGF0ZS5zcHJpdGVzLnNwaWRlciA9IHNwaWRlcjtcclxufVxyXG5cclxubW9kdWxlLmV4cG9ydHMgPSB7XHJcbiAgICBzZXR1cEVuZW15LFxyXG4gICAgcGxhY2VFbmVteSxcclxufVxyXG5cclxuXHJcblxyXG4vLyB2YXIgZW5lbXkgPSBbXCJiYXRcIiwgXCJoZWRnZS1ob2dcIiwgXCJjYWN0dXNcIiwgXCJzcGlkZXJcIl1cclxuLy8gdmFyIGVuZW15QmFsYW5jaW5nID0ge1xyXG4vLyAgICAgXCJoZWRnZS1ob2dcIjoge1xyXG4vLyAgICAgICAgIHNjYWxlOiB7XHJcbi8vICAgICAgICAgICAgIHg6IDIsXHJcbi8vICAgICAgICAgICAgIHk6IDIsXHJcbi8vICAgICAgICAgfSxcclxuLy8gICAgICAgICBtb3ZlbWVudFNwZWVkOiAkLnN0YXRlLnNjZW5lLnJvYWQubW92ZW1lbnRTcGVlZCxcclxuLy8gICAgICAgICBwb3NpdGlvbjoge1xyXG4vLyAgICAgICAgICAgICB4OiAkLnN0YWdlV2lkdGggKyAyMCxcclxuLy8gICAgICAgICAgICAgeTogJC5zdGF0ZS5zY2VuZS5yb2FkLnBvc2l0aW9uLnksXHJcbi8vICAgICAgICAgfSxcclxuLy8gICAgICAgICBwaXZvdDoge1xyXG4vLyAgICAgICAgICAgICB4OiAwLFxyXG4vLyAgICAgICAgICAgICB5OiAyMDAsXHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgfSxcclxuICAgIFxyXG4vLyAgICAgXCJzcGlkZXJcIjoge1xyXG4vLyAgICAgICAgIHNjYWxlOiB7XHJcbi8vICAgICAgICAgICAgIHg6IDIsXHJcbi8vICAgICAgICAgICAgIHk6IDIsXHJcbi8vICAgICAgICAgfSxcclxuLy8gICAgICAgICBtb3ZlbWVudFNwZWVkOiAkLnN0YXRlLnNjZW5lLnJvYWQubW92ZW1lbnRTcGVlZCxcclxuLy8gICAgICAgICBwb3NpdGlvbjoge1xyXG4vLyAgICAgICAgICAgICB4OiAkLnN0YWdlV2lkdGggKyAyMCxcclxuLy8gICAgICAgICAgICAgeTogJC5zdGF0ZS5zY2VuZS5yb2FkLnBvc2l0aW9uLnksXHJcbi8vICAgICAgICAgfSxcclxuLy8gICAgICAgICBwaXZvdDoge1xyXG4vLyAgICAgICAgICAgICB4OiAwLFxyXG4vLyAgICAgICAgICAgICB5OiAyMDAsXHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgfSxcclxuXHJcbi8vICAgICBcImNhY3R1c1wiOiB7XHJcblxyXG4vLyAgICAgfVxyXG4vLyB9OyIsInZhciAkID0gcmVxdWlyZShcIi4vdXRpbC5qc1wiKTtcclxudmFyICRzY2VuZSA9IHJlcXVpcmUoXCIuL3NjZW5lLmpzXCIpO1xyXG52YXIgJHBsYXllciA9IHJlcXVpcmUoXCIuL3BsYXllci5qc1wiKTtcclxudmFyICRlbmVteSA9IHJlcXVpcmUoXCIuL2VuZW15LmpzXCIpO1xyXG5cclxuKGZ1bmN0aW9uKCl7XHJcbiAgICAkLnJlbmRlcmVyLmJhY2tncm91bmRDb2xvciA9IDB4ZmZmZmZmO1xyXG4gICAgJC5zdGFnZS5pbnRlcmFjdGl2ZSA9IHRydWU7XHJcblxyXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlkb3duXCIsIGV2ZW50ID0+IG9uS2V5RG93bihldmVudCkpO1xyXG5cclxuICAgIGxvYWRSZXNvdXJjZSgpO1xyXG4gICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZCgkLnJlbmRlcmVyLnZpZXcpO1xyXG59KCkpO1xyXG5cclxuZnVuY3Rpb24gbG9hZFJlc291cmNlKCkge1xyXG4gICAgJC5sb2FkZXIuYWRkKCcvc3ByaXRlcy9zcHJpdGVzLmpzb24nKTtcclxuICAgICQubG9hZGVyLm9uKCdwcm9ncmVzcycsIChfbG9hZGVyLCByZXNvdXJjZSkgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGBMb2FkaW5nOiAke3Jlc291cmNlLm5hbWV9ICgke19sb2FkZXIucHJvZ3Jlc3N9JSlgKTtcclxuICAgIH0pO1xyXG4gICAgJC5sb2FkZXIubG9hZCgobG9hZGVyLCByZXNvdXJjZXMpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkFsbCBmaWxlcyBsb2FkZWQgLi4uXCIpO1xyXG4gICAgICAgICRzY2VuZS5zZXR1cFNjZW5lKCk7XHJcbiAgICAgICAgJHBsYXllci5zZXR1cFBsYXllcigpO1xyXG4gICAgICAgICRlbmVteS5zZXR1cEVuZW15KCk7XHJcbiAgICAgICAgdXBkYXRlKCk7XHJcbiAgICB9KTtcclxuICAgICQubG9hZGVyLm9uQ29tcGxldGUuYWRkKCgpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkxvYWRpbmcgQ29tcGxldGUgLi4uXCIpXHJcbiAgICAgICAgJC5zdGFnZS5hZGRDaGlsZCgkLnNjZW5lKTtcclxuICAgICAgICAkLnN0YWdlLmFkZENoaWxkKCQucGxheWVyKTtcclxuXHJcbiAgICAgICAgJHBsYXllci53YWxraW5nUGxheWVyKCk7XHJcbiAgICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gb25LZXlEb3duIChldmVudCkge1xyXG4gICAgc3dpdGNoKGV2ZW50LmtleUNvZGUpIHtcclxuICAgICAgICAvLyBjYXNlIDQwOlxyXG4gICAgICAgIC8vICAgICByb2xsaW5nUGxheWVyKCk7XHJcbiAgICAgICAgLy8gICAgIGJyZWFrO1xyXG4gICAgICAgIC8vIGNhc2UgMzg6XHJcbiAgICAgICAgLy8gICAgIGp1bXBpbmdQbGF5ZXIoKTtcclxuICAgICAgICAvLyAgICAgYnJlYWs7XHJcbiAgICAgICAgY2FzZSAzOTpcclxuICAgICAgICAgICAgLy8gUmlnaHQgYXJyb3cga2V5XHJcbiAgICAgICAgICAgICRwbGF5ZXIuc2hvb3RpbmdQbGF5ZXIoKTtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgY2FzZSAzNzpcclxuICAgICAgICAgICAgLy8gTGVmdCBhcnJvdyBrZXlcclxuICAgICAgICAgICAgJGVuZW15LnBsYWNlRW5lbXkoKTtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICB9XHJcbiAgICBcclxufVxyXG5cclxuZnVuY3Rpb24gdXBkYXRlKCkge1xyXG4gICBcclxuXHJcbiAgICAkLnVwZGF0ZVNjZW5lKCk7XHJcbiAgICAkLnVwZGF0ZVBsYXllclBvc2l0aW9uKCk7XHJcblxyXG4gICAgJC5yZW5kZXJlci5yZW5kZXIoJC5zdGFnZSk7XHJcbiAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKHVwZGF0ZSk7XHJcbiAgICBcclxufSIsInZhciAkID0gcmVxdWlyZShcIi4vdXRpbC5qc1wiKTtcclxuXHJcbmZ1bmN0aW9uIHdhbGtpbmdQbGF5ZXIgKCkge1xyXG4gICAgJC5zZXRQbGF5ZXJBbmltYXRpb24oJC5hbmltYXRpb25TdGF0ZS5SVU5OSU5HKTtcclxuICAgIGxldCB0ZXh0dXJlQXJyYXlSdW5uaW5nID0gW107XHJcblxyXG4gICAgZm9yIChsZXQgaSA9IDE7IGkgPD0gMjsgaSsrKSB7XHJcbiAgICAgICAgdGV4dHVyZUFycmF5UnVubmluZy5wdXNoKCQuVGV4dHVyZUNhY2hlWyd3YWxrJytpKycucG5nJ10pO1xyXG4gICAgfVxyXG5cclxuICAgIGxldCB3YWxraW5nU3ByaXRlID0gbmV3IFBJWEkuZXh0cmFzLkFuaW1hdGVkU3ByaXRlKHRleHR1cmVBcnJheVJ1bm5pbmcpO1xyXG4gICAgXHJcbiAgICB3YWxraW5nU3ByaXRlLmFuaW1hdGlvblNwZWVkID0gKC4xMCArICQuc3RhdGUudGltZVNsb3cpO1xyXG4gICAgd2Fsa2luZ1Nwcml0ZS5wbGF5KCk7XHJcblxyXG4gICAgZmx1c2hQbGF5ZXJDb250YWluZXIoKTtcclxuICAgICQucGxheWVyLmFkZENoaWxkKHdhbGtpbmdTcHJpdGUpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBzaG9vdGluZ1BsYXllciAoKSB7XHJcbiAgICBpZiAoJC5zdGF0ZS5wbGF5ZXJJbmZvLmFjdGl2ZUFuaW1hdGlvbiAhPT0gJC5hbmltYXRpb25TdGF0ZS5SVU5OSU5HKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGxldCBkYXJ0cyA9ICQuc3RhdGUuZGFydHM7XHJcbiAgICBpZiAoZGFydHMuYWN0aXZlRGFydHMgPiAxKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnNvbGUubG9nKFwiZGFydHMuYWN0aXZlRGFydHNcIiwgJC5zY2VuZS5jaGlsZHJlbilcclxuXHJcbiAgICBkYXJ0cy5hY3RpdmVEYXJ0cysrO1xyXG4gICAgJC5zZXRTdGF0ZSh7ZGFydHN9KTtcclxuXHJcbiAgICAkLnNldFBsYXllckFuaW1hdGlvbigkLmFuaW1hdGlvblN0YXRlLlNIT09USU5HKTtcclxuXHJcbiAgICBsZXQgdGV4dHVyZUFycmF5U2hvb3RpbmcgPSBbXTtcclxuICAgIGZvciAobGV0IGkgPSAxOyBpIDw9IDI7IGkrKykge1xyXG4gICAgICAgIHRleHR1cmVBcnJheVNob290aW5nLnB1c2goJC5UZXh0dXJlQ2FjaGVbJ3Nob290LnBuZyddKTtcclxuICAgIH1cclxuXHJcbiAgICBsZXQgc2hvb3RpbmdTcHJpdGUgPSBuZXcgUElYSS5leHRyYXMuQW5pbWF0ZWRTcHJpdGUodGV4dHVyZUFycmF5U2hvb3RpbmcpO1xyXG5cclxuICAgIHNob290aW5nU3ByaXRlLmFuaW1hdGlvblNwZWVkID0gKC4wMSArICQuc3RhdGUudGltZVNsb3cpO1xyXG4gICAgc2hvb3RpbmdTcHJpdGUucGxheSgpO1xyXG5cclxuICAgIGZsdXNoUGxheWVyQ29udGFpbmVyKCk7XHJcbiAgICAkLnBsYXllci5hZGRDaGlsZChzaG9vdGluZ1Nwcml0ZSk7XHJcblxyXG4gICAgc2hvb3REYXJ0KCk7XHJcbiAgICB3YWxraW5nUGxheWVyKCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHNob290RGFydCAoKSB7XHJcblxyXG4gICAgbGV0IGRhcnQgPSBuZXcgJC5TcHJpdGUoJC5UZXh0dXJlQ2FjaGVbXCJkYXJ0LnBuZ1wiXSk7XHJcbiAgICBsZXQgZGFydHNwZWVkID0gJC5zdGF0ZS5kYXJ0cy5kYXJ0c3BlZWQ7XHJcbiAgICBcclxuICAgIGxldCBkYXJ0U2NhbGVQb2ludCA9IG5ldyBQSVhJLlBvaW50KDAuNSwgMC41KTtcclxuICAgIGRhcnQuc2NhbGUgPSBkYXJ0U2NhbGVQb2ludDtcclxuXHJcbiAgICBsZXQgcGxheWVyUm90YXRpb24gPSAkLnBsYXllci5yb3RhdGlvbjtcclxuICAgIGxldCBkYXJ0U3RhcnRQb3NpdGlvbiA9IHtcclxuICAgICAgICB4OiAkLnBsYXllci5wb3NpdGlvbi54ICsgTWF0aC5jb3MocGxheWVyUm90YXRpb24pKjIwLFxyXG4gICAgICAgIHk6ICQucGxheWVyLnBvc2l0aW9uLnkgKyBNYXRoLnNpbihwbGF5ZXJSb3RhdGlvbikqMjBcclxuICAgIH07XHJcblxyXG4gICAgZGFydC5wb3NpdGlvbi54ID0gZGFydFN0YXJ0UG9zaXRpb24ueCArIDIwO1xyXG4gICAgZGFydC5wb3NpdGlvbi55ID0gZGFydFN0YXJ0UG9zaXRpb24ueSAtIDQwO1xyXG4gICAgZGFydC5yb3RhdGlvbiA9IHBsYXllclJvdGF0aW9uO1xyXG5cclxuICAgIHZhciBzbW9vdGhpZSA9IG5ldyBTbW9vdGhpZSh7XHJcbiAgICAgICAgZW5naW5lOiBQSVhJLCBcclxuICAgICAgICByZW5kZXJlcjogJC5yZW5kZXJlcixcclxuICAgICAgICByb290OiAkLnN0YWdlLFxyXG4gICAgICAgIGZwczogMzAsXHJcbiAgICAgICAgdXBkYXRlOiB1cGRhdGUuYmluZCh0aGlzKVxyXG4gICAgfSk7XHJcblxyXG4gICAgXHJcbiAgICAkLnN0YWdlLmFkZENoaWxkKGRhcnQpO1xyXG4gICAgc21vb3RoaWUuc3RhcnQoKTtcclxuXHJcbiAgICBmdW5jdGlvbiB1cGRhdGUgKCkge1xyXG4gICAgICAgIGlmIChkYXJ0LnBvc2l0aW9uLnggPiAkLnN0YWdlV2lkdGgpIHtcclxuICAgICAgICAgICAgJC5zdGFnZS5yZW1vdmVDaGlsZChkYXJ0KTtcclxuXHJcbiAgICAgICAgICAgIGxldCBkYXJ0cyA9ICQuc3RhdGUuZGFydHM7XHJcbiAgICAgICAgICAgIGRhcnRzLmFjdGl2ZURhcnRzIC0tO1xyXG4gICAgICAgICAgICAkLnNldFN0YXRlKHtkYXJ0c30pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGRhcnQucG9zaXRpb24ueCArPSBNYXRoLmNvcyhkYXJ0LnJvdGF0aW9uKSAqIGRhcnRzcGVlZDtcclxuICAgICAgICAgICAgZGFydC5wb3NpdGlvbi55ICs9IE1hdGguc2luKGRhcnQucm90YXRpb24pICogZGFydHNwZWVkO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuLy8gUGxheWVyIHV0aWxpdHkgZnVuY3Rpb24gYmVsb3c6XHJcblxyXG5mdW5jdGlvbiBmbHVzaFBsYXllckNvbnRhaW5lcigpIHtcclxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgJC5wbGF5ZXIuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAkLnBsYXllci5yZW1vdmVDaGlsZEF0KGkpO1xyXG4gICAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBzZXR1cFBsYXllciAoKSB7XHJcbiAgICAkLnBsYXllci5hbmNob3IueCA9IDAuNTtcclxuICAgICQucGxheWVyLmFuY2hvci55ID0gMC41O1xyXG5cclxuICAgICQucGxheWVyLnBpdm90LnggPSAwLjU7XHJcbiAgICAkLnBsYXllci5waXZvdC55ID0gMjAwOyAvLyAyMDBcclxuXHJcbiAgICAkLnBsYXllci5zY2FsZS54ICA9IDAuMzA7XHJcbiAgICAkLnBsYXllci5zY2FsZS55ICA9IDAuMzA7XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0ge1xyXG4gICAgc2V0dXBQbGF5ZXIsXHJcbiAgICByZWZyZXNoUGxheWVyOiBzZXR1cFBsYXllcixcclxuXHJcbiAgICB3YWxraW5nUGxheWVyLFxyXG4gICAgZGVmYXVsdFBsYXllcjogd2Fsa2luZ1BsYXllcixcclxuICAgIHNob290aW5nUGxheWVyLFxyXG59IiwidmFyICQgPSByZXF1aXJlKFwiLi91dGlsLmpzXCIpO1xyXG5cclxuZnVuY3Rpb24gc2V0dXBTY2VuZSAoKSB7XHJcbiAgICBsZXQgc2NlbmUgPSAkLnN0YXRlLnNjZW5lO1xyXG5cclxuICAgIGxldCBoaWxscyA9ICQuVGV4dHVyZUNhY2hlW1wiaGlsbHMucG5nXCJdO1xyXG4gICAgJC5zdGF0ZS5zcHJpdGVzLmhpbGxzID0gbmV3IFBJWEkuVGlsaW5nU3ByaXRlKGhpbGxzLCAxNTEzLCAyNTIpO1xyXG4gICAgYWRkVG9TY2VuZSgkLnN0YXRlLnNwcml0ZXMuaGlsbHMsIHNjZW5lLmhpbGxzLnBvc2l0aW9uLCB7eDogMCwgeTogMH0pO1xyXG5cclxuICAgIGxldCBjbG91ZHMgPSAkLlRleHR1cmVDYWNoZVtcImNsb3Vkcy5wbmdcIl07XHJcbiAgICAkLnN0YXRlLnNwcml0ZXMuY2xvdWRzID0gbmV3IFBJWEkuVGlsaW5nU3ByaXRlKGNsb3VkcywgMTUxMywgMTAwKTtcclxuICAgIGFkZFRvU2NlbmUoJC5zdGF0ZS5zcHJpdGVzLmNsb3Vkcywgc2NlbmUuY2xvdWRzLnBvc2l0aW9uLCB7eDogMCwgeTogMH0pO1xyXG5cclxuICAgIGxldCByb2FkID0gJC5UZXh0dXJlQ2FjaGVbXCJyb2FkLnBuZ1wiXTtcclxuICAgICQuc3RhdGUuc3ByaXRlcy5yb2FkID0gbmV3IFBJWEkuVGlsaW5nU3ByaXRlKHJvYWQsIDE1MTMsIDUyKTtcclxuICAgIGFkZFRvU2NlbmUoJC5zdGF0ZS5zcHJpdGVzLnJvYWQsIHNjZW5lLnJvYWQucG9zaXRpb24sIHt4OiAwLCB5OiAwfSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGFkZFRvU2NlbmUoc3ByaXRlLCBwb3NpdGlvbiwgdGlsZVBvc2l0aW9uID0gbnVsbCkge1xyXG4gICAgc3ByaXRlLnBvc2l0aW9uLnggPSBwb3NpdGlvbi54O1xyXG4gICAgc3ByaXRlLnBvc2l0aW9uLnkgPSBwb3NpdGlvbi55O1xyXG5cclxuICAgIGlmICh0aWxlUG9zaXRpb24gIT09IG51bGwgKSB7XHJcbiAgICAgICAgc3ByaXRlLnRpbGVQb3NpdGlvbi54ID0gdGlsZVBvc2l0aW9uLng7XHJcbiAgICAgICAgc3ByaXRlLnRpbGVQb3NpdGlvbi55ID0gdGlsZVBvc2l0aW9uLnk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgICQuc2NlbmUuYWRkQ2hpbGQoc3ByaXRlKTtcclxufVxyXG5cclxubW9kdWxlLmV4cG9ydHMgPSB7XHJcbiAgICBzZXR1cFNjZW5lLFxyXG59IiwidmFyXHJcbnN0YWdlV2lkdGggICAgICAgICAgICAgID0gNzAwLFxyXG5zdGFnZUhlaWdodCAgICAgICAgICAgICA9IDQwMCxcclxuQ29udGFpbmVyICAgICAgICAgICAgICAgPSBQSVhJLkNvbnRhaW5lcixcclxuVGV4dHVyZUNhY2hlICAgICAgICAgICAgPSBQSVhJLnV0aWxzLlRleHR1cmVDYWNoZSxcclxuVGlsaW5nU3ByaXRlICAgICAgICAgICAgPSBQSVhJLlRpbGluZ1Nwcml0ZSxcclxuU3ByaXRlICAgICAgICAgICAgICAgICAgPSBQSVhJLlNwcml0ZSxcclxuYXV0b0RldGVjdFJlbmRlcmVyICAgICAgPSBQSVhJLmF1dG9EZXRlY3RSZW5kZXJlcixcclxubG9hZGVyICAgICAgICAgICAgICAgICAgPSBQSVhJLmxvYWRlcixcclxucmVzb3VyY2VzICAgICAgICAgICAgICAgPSBQSVhJLmxvYWRlci5yZXNvdXJjZXMsXHJcbnJlbmRlcmVyICAgICAgICAgICAgICAgID0gYXV0b0RldGVjdFJlbmRlcmVyKHN0YWdlV2lkdGgsIHN0YWdlSGVpZ2h0KSxcclxuc3RhZ2UgICAgICAgICAgICAgICAgICAgPSBuZXcgQ29udGFpbmVyKCksXHJcbnNjZW5lICAgICAgICAgICAgICAgICAgID0gbmV3IENvbnRhaW5lcigpLFxyXG5wbGF5ZXIgICAgICAgICAgICAgICAgICA9IG5ldyBTcHJpdGUoKTtcclxuXHJcbmNvbnN0IGFuaW1hdGlvblN0YXRlID0ge1xyXG4gICAgUlVOTklORzogXCJSVU5OSU5HXCIsXHJcbiAgICBKVU1QSU5HOiBcIkpVTVBJTkdcIixcclxuICAgIFJPTExJTkc6IFwiUk9MTElOR1wiLFxyXG4gICAgU0hPT1RJTkc6IFwiU0hPT1RJTkdcIixcclxuICAgIFNMT1c6IFwiU0xPV1wiLFxyXG59O1xyXG5cclxudmFyIGdhbWVTdGF0ZSA9IHtcclxuICAgIHNwcml0ZXM6IHt9LFxyXG4gICAgdGltZVNsb3c6IDAsXHJcbiAgICB0aW1lcnM6IFtdLFxyXG4gICAgZGFydHM6IHtcclxuICAgICAgICBhY3RpdmVEYXJ0czogMCxcclxuICAgICAgICBkYXJ0c3BlZWQ6IDEwLFxyXG4gICAgfSxcclxuICAgIGFjdGl2ZUVuZW1pZXM6IFtdLFxyXG4gICAgcGxheWVySW5mbzoge1xyXG4gICAgICAgIGFjdGl2ZUFuaW1hdGlvbjogYW5pbWF0aW9uU3RhdGUuUlVOTklORywgLy8gYWN0aXZlIHBsYXllciBhbmltYXRpb24gc3RhdGVcclxuICAgICAgICBqdW1wRGVsdGE6IDUsXHJcbiAgICAgICAgcm9sbERlbHRhOiAxNSxcclxuICAgICAgICBoZWlnaHQ6IDAsXHJcbiAgICAgICAgd2lkdGg6IDAsXHJcbiAgICAgICAgcG9zaXRpb246e1xyXG4gICAgICAgICAgICB4OiAxMDAsXHJcbiAgICAgICAgICAgIHk6IDI1MSAvLyBgcm9hZGAgcG9zaWl0b24geVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBzY2VuZToge1xyXG4gICAgICAgIHJvYWQ6IHtcclxuICAgICAgICAgICAgcG9zaXRpb246IHtcclxuICAgICAgICAgICAgICAgIHg6IDAsXHJcbiAgICAgICAgICAgICAgICB5OiAyNTEsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIG1vdmVtZW50U3BlZWQ6IDJcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNsb3Vkczoge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjoge1xyXG4gICAgICAgICAgICAgICAgeDogMCxcclxuICAgICAgICAgICAgICAgIHk6IDAsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIG1vdmVtZW50U3BlZWQ6IDAuM1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaGlsbHM6IHtcclxuICAgICAgICAgICAgcG9zaXRpb246IHtcclxuICAgICAgICAgICAgICAgIHg6IDAsXHJcbiAgICAgICAgICAgICAgICB5OiAwLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBtb3ZlbWVudFNwZWVkOiAwLjJcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbnZhciBzdGF0ZSA9IGdhbWVTdGF0ZTtcclxudmFyIHNldFN0YXRlID0gKGNoYW5nZSA9IG51bGwpID0+IHtcclxuICAgIGlmKCFjaGFuZ2UpIHtcclxuICAgICAgICBzdGF0ZSA9IGdhbWVTdGF0ZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc3RhdGUgPSBPYmplY3QuYXNzaWduKHt9LCBzdGF0ZSwgY2hhbmdlKTtcclxuICAgIH1cclxuICAgIHJldHVybjtcclxufVxyXG5cclxudmFyIHNldFBsYXllckFuaW1hdGlvbiA9IChhbmltYXRpb25TdGF0ZSkgPT4ge1xyXG4gICAgbGV0IHBsYXllckluZm8gPSBzdGF0ZS5wbGF5ZXJJbmZvO1xyXG4gICAgcGxheWVySW5mby5hY3RpdmVBbmltYXRpb24gPSBhbmltYXRpb25TdGF0ZTtcclxufVxyXG5cclxudmFyIHVwZGF0ZVBsYXllclBvc2l0aW9uID0gKCkgPT4ge1xyXG4gICAgbGV0IHBsYXllckluZm8gPSBzdGF0ZS5wbGF5ZXJJbmZvO1xyXG4gICAgXHJcbiAgICBwbGF5ZXIucG9zaXRpb24ueCA9IHBsYXllckluZm8ucG9zaXRpb24ueDtcclxuICAgIHBsYXllci5wb3NpdGlvbi55ID0gcGxheWVySW5mby5wb3NpdGlvbi55O1xyXG59XHJcblxyXG5cclxudmFyIHVwZGF0ZVNjZW5lID0gKCkgPT4ge1xyXG4gICAgbGV0IHNjZW5lID0gc3RhdGUuc2NlbmU7XHJcblxyXG4gICAgc3RhdGUuc3ByaXRlcy5oaWxscy50aWxlUG9zaXRpb24ueCAtPSAoc2NlbmUuaGlsbHMubW92ZW1lbnRTcGVlZCArIHN0YXRlLnRpbWVTbG93KTtcclxuICAgIHN0YXRlLnNwcml0ZXMuY2xvdWRzLnRpbGVQb3NpdGlvbi54IC09IChzY2VuZS5jbG91ZHMubW92ZW1lbnRTcGVlZCArIHN0YXRlLnRpbWVTbG93KTtcclxuICAgIHN0YXRlLnNwcml0ZXMucm9hZC50aWxlUG9zaXRpb24ueCAtPSAoc2NlbmUucm9hZC5tb3ZlbWVudFNwZWVkICsgc3RhdGUudGltZVNsb3cpO1xyXG59XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IHtcclxuICAgIHN0YWdlV2lkdGggICAgICAgICAgICAgICxcclxuICAgIHN0YWdlSGVpZ2h0ICAgICAgICAgICAgICxcclxuICAgIENvbnRhaW5lciAgICAgICAgICAgICAgICxcclxuICAgIFRleHR1cmVDYWNoZSAgICAgICAgICAgICxcclxuICAgIFRpbGluZ1Nwcml0ZSAgICAgICAgICAgICxcclxuICAgIFNwcml0ZSAgICAgICAgICAgICAgICAgICxcclxuICAgIGF1dG9EZXRlY3RSZW5kZXJlciAgICAgICxcclxuICAgIGxvYWRlciAgICAgICAgICAgICAgICAgICxcclxuICAgIHJlbmRlcmVyICAgICAgICAgICAgICAgICxcclxuICAgIHJlc291cmNlcyAgICAgICAgICAgICAgICxcclxuICAgIHN0YWdlICAgICAgICAgICAgICAgICAgICxcclxuICAgIHNjZW5lICAgICAgICAgICAgICAgICAgICxcclxuICAgIHBsYXllciAgICAgICAgICAgICAgICAgICxcclxuICAgIGFuaW1hdGlvblN0YXRlICAgICAgICAgICxcclxuICAgIHN0YXRlICAgICAgICAgICAgICAgICAgICxcclxuICAgIHNldFN0YXRlICAgICAgICAgICAgICAgICxcclxuICAgIHNldFBsYXllckFuaW1hdGlvbiAgICAgICxcclxuICAgIHVwZGF0ZVBsYXllclBvc2l0aW9uICAgICxcclxuICAgIHVwZGF0ZVNjZW5lICAgICAgICAgICAgICxcclxufSJdfQ==
